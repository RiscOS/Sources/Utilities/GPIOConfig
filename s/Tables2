;
; Copyright (c) 2011, Tank Stage Lighting
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of the copyright holder nor the names of their
;       contributors may be used to endorse or promote products derived from
;       this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;

;s.Tables2
non_table
	DCD	-1		;0
;beagle_table
	DCD	XMtable2	;1
;beagle1_table
	DCD	XMtable2	;2
;beagle2_table
	DCD	XMtable2	;3
;beagle3_table
	DCD	XMtable2	;4
;beagle4_table
	DCD	XMtable2	;5
;beagle5_table
	DCD	XMtable2	;6
;devkit_table
	DCD	DEVtable2	;7
;igep_table
	DCD	XMtable2	;8
;igep1_table
	DCD	XMtable2	;9
;pandora_table
	DCD	Doratable2	;10
;panda_table
	DCD	PDtable2	;11
;pandaes_table
	DCD	PDtable2ES	;12
;rpi_table
	DCD	RPItable2R1	;13
;rpi1_table
	DCD	RPItable2R2	;14
;rpi2_table
	DCD	RPItable2R2	;15
;rpi3_table
	DCD	RPItable2BP	;16
;rpicompute_table
	DCD	RPItable2Comp	;17
;rpicompute3_table
	DCD	RPItable2Comp	;18
;rpiAP_table
	DCD	RPItable2BP	;19
;rpiMk2B_table
	DCD	RPItable2BP1	;20
;rpizero_table
	DCD	RPItable2BP1	;21
;rpizerow_table
	DCD	RPItable2BP1	;22
;rpiMk3B_table
	DCD	RPItable2BP1	;23
;rpiMk3BP_table
	DCD	RPItable2BP1	;24
;rpiMk3AP_table
	DCD	RPItable2BP1	;25
;rpicompute3P_table
	DCD	RPItable2Comp	;26
;rpiMk4B_table
	DCD	RPItable2BP1	;27
;omaptable
	DCD	omap5table	;28
;uevmtable
	DCD	omap5table	;29
;titaniumtable
	DCD	-1		;30
;rpizero2w_table
	DCD	RPItable2BP1	;31
;rpi400_table
	DCD	RPItable2BP1	;32
;rpicompute4_table
	DCD	RPItable2BP1	;33

;*************************** Beagle Xm **************************************
M_button	*	gpio_4
M_led0		*	gpio_149
M_led1		*	gpio_150
;Expansion GPIO's pin table
XMtable2
	DCB	gpio_139
	DCB	gpio_144
	DCB	gpio_138
	DCB	gpio_146
	DCB	gpio_137
	DCB	gpio_143
	DCB	gpio_136
	DCB	gpio_145
	DCB	gpio_135
	DCB	gpio_158
	DCB	gpio_134
	DCB	gpio_162
	DCB	gpio_133
	DCB	gpio_161
	DCB	gpio_132
	DCB	gpio_159
	DCB	gpio_131
	DCB	gpio_156
	DCB	gpio_130
	DCB	gpio_157
	DCB	gpio_183
	DCB	gpio_168
;Auxillary GPIO's pin table
	DCB	gpio_20
	DCB	gpio_21
	DCB	gpio_17
	DCB	gpio_16
	DCB	gpio_15
	DCB	gpio_19
	DCB	gpio_23
	DCB	gpio_14
	DCB	gpio_18
	DCB	gpio_13
	DCB	gpio_22
	DCB	gpio_12
	DCB	gpio_170
	DCB	gpio_57
;camera table
	DCB	gpio_110
	DCB	gpio_96
	DCB	gpio_109
	DCB	gpio_108	;INPUT ONLY
	DCB	gpio_183
	DCB	gpio_107	;INPUT ONLY
	DCB	gpio_168
	DCB	gpio_106	;INPUT ONLY
	DCB	gpio_105	;INPUT ONLY
	DCB	gpio_167
	DCB	gpio_104
	DCB	gpio_103
	DCB	gpio_102
	DCB	gpio_101
	DCB	gpio_100	;INPUT ONLY
	DCB	gpio_99		;INPUT ONLY
	DCB	gpio_97
	DCB	gpio_94
	DCB	gpio_95
;user table
	DCB	M_button
	DCB	M_led0
	DCB	M_led1
	DCB	&FF
	ALIGN
;************************* Beagle Xm End ************************************
;**************************** Devkit ****************************************
dev_button	*	gpio_26
dev_led1	*	gpio_186
dev_led2	*	gpio_163
dev_led3	*	gpio_164
;expansion GPIO's pin table
DEVtable2
	DCB	gpio_158
	DCB	gpio_159
	DCB	gpio_156
	DCB	gpio_161
	DCB	gpio_162
	DCB	gpio_160
	DCB	gpio_157
	DCB	gpio_150
	DCB	gpio_149
	DCB	gpio_151
	DCB	gpio_148
	DCB	gpio_130
	DCB	gpio_131
	DCB	gpio_132
	DCB	gpio_133
	DCB	gpio_134
	DCB	gpio_135
	DCB	gpio_136
	DCB	gpio_137
	DCB	gpio_138
	DCB	gpio_139
	DCB	gpio_140
	DCB	gpio_141
	DCB	gpio_142
	DCB	gpio_143
	DCB	gpio_184
	DCB	gpio_185
	DCB	gpio_172
	DCB	gpio_173
	DCB	gpio_171
	DCB	gpio_174
	DCB	gpio_177
	DCB	gpio_170
;user bits table
	DCB	dev_button
	DCB	dev_led1
	DCB	dev_led2
	DCB	dev_led3
	DCB	&FF
	ALIGN
;**************************** Devkit End *************************************
;**************************** Pandora ****************************************
;Expansion GPIO's pin table
Doratable2
	DCB	gpio_145
	DCB	gpio_147
	DCB	gpio_144
	DCB	gpio_166
	DCB	gpio_146
	DCB	gpio_165
;Internal GPIO's pin table
	DCB	gpio_58
	DCB	gpio_64
	DCB	gpio_65
	DCB	gpio_95
	DCB	gpio_97
	DCB	gpio_107
	DCB	gpio_167
	DCB	gpio_170
	DCB	&FF
	ALIGN

;**************************** Pandora End****************************************
;**************************** Panda ****************************************
PDES_button	*	gpio_113
PDES_ledD1	*	gpio_110
PD_button	*	gpio_121
PD_ledD1	*	gpio_7
PD_ledD2	*	gpio_8
;Expansion GPIO's pin table
PDtable2
	DCB	gpio_38
	DCB	gpio_37
	DCB	gpio_36
	DCB	gpio_32
	DCB	gpio_61
	DCB	gpio_33
	DCB	gpio_54
	DCB	gpio_34
	DCB	gpio_55
	DCB	gpio_35
	DCB	gpio_50
	DCB	gpio_56
	DCB	gpio_51
	DCB	gpio_59
;Auxillary GPIO's pin table
	DCB	gpio_140
	DCB	gpio_156
	DCB	gpio_155
	DCB	gpio_138
	DCB	gpio_136
	DCB	gpio_139
	DCB	gpio_137
	DCB	gpio_135
	DCB	gpio_134
	DCB	gpio_39
	DCB	gpio_133
	DCB	gpio_132
;camera table
	DCB	gpio_67
	DCB	gpio_73
	DCB	gpio_68
	DCB	gpio_74
	DCB	gpio_69
	DCB	gpio_75
	DCB	gpio_70
	DCB	gpio_76
	DCB	gpio_71
	DCB	gpio_40
	DCB	gpio_72
	DCB	gpio_45
	DCB	gpio_83
	DCB	gpio_130
	DCB	gpio_81
	DCB	gpio_131
	DCB	gpio_82
	DCB	gpio_47
	DCB	gpio_44
	DCB	gpio_181
	DCB	gpio_42
;user table
	DCB	PD_button
	DCB	PD_ledD1
	DCB	PD_ledD2
	DCB	&FF
	ALIGN


;Expansion GPIO's pin table
PDtable2ES
	DCB	gpio_38
	DCB	gpio_37
	DCB	gpio_121
	DCB	gpio_36
	DCB	gpio_32
	DCB	gpio_61
	DCB	gpio_33
	DCB	gpio_54
	DCB	gpio_34
	DCB	gpio_55
	DCB	gpio_35
	DCB	gpio_50
	DCB	gpio_56
	DCB	gpio_51
	DCB	gpio_59
;Auxillary GPIO's pin table
	DCB	gpio_140
	DCB	gpio_156
	DCB	gpio_155
	DCB	gpio_138
	DCB	gpio_136
	DCB	gpio_139
	DCB	gpio_137
	DCB	gpio_135
	DCB	gpio_134
	DCB	gpio_39
	DCB	gpio_133
	DCB	gpio_132
;camera table
	DCB	gpio_67
	DCB	gpio_73
	DCB	gpio_68
	DCB	gpio_74
	DCB	gpio_69
	DCB	gpio_75
	DCB	gpio_70
	DCB	gpio_76
	DCB	gpio_71
	DCB	gpio_40
	DCB	gpio_72
	DCB	gpio_45
	DCB	gpio_83
	DCB	gpio_130
	DCB	gpio_81
	DCB	gpio_131
	DCB	gpio_82
	DCB	gpio_47
	DCB	gpio_44
	DCB	gpio_181
	DCB	gpio_42
;user table
	DCB	PDES_button
	DCB	PDES_ledD1
	DCB	PD_ledD2
	DCB	&FF
	ALIGN
;**************************** Pandora End ************************************
;**************************** Raspberry ****************************************
Pi_LED		*	gpio_16
Com_LED		*	gpio_47
Pi4_LED		*	gpio_42
;expansion GPIO's pin table
RPItable2R1
	DCB	gpio_0
	DCB	gpio_1
	DCB	gpio_4
	DCB	gpio_14
	DCB	gpio_15
	DCB	gpio_17
	DCB	gpio_18
	DCB	gpio_21
	DCB	gpio_22
	DCB	gpio_23
	DCB	gpio_24
	DCB	gpio_10
	DCB	gpio_9
	DCB	gpio_25
	DCB	gpio_11
	DCB	gpio_8
	DCB	gpio_7
;user bits table
	DCB	Pi_LED
	DCB	&FF
	ALIGN

;expansion GPIO's pin table
RPItable2R2
	DCB	gpio_2
	DCB	gpio_3
	DCB	gpio_4
	DCB	gpio_14
	DCB	gpio_15
	DCB	gpio_17
	DCB	gpio_18
	DCB	gpio_27
	DCB	gpio_22
	DCB	gpio_23
	DCB	gpio_24
	DCB	gpio_10
	DCB	gpio_9
	DCB	gpio_25
	DCB	gpio_11
	DCB	gpio_8
	DCB	gpio_7
;Auxillary GPIO's pin table
	DCB	gpio_28
	DCB	gpio_29
	DCB	gpio_30
	DCB	gpio_31
;user bits table
	DCB	Pi_LED
	DCB	&FF
	ALIGN

;expansion GPIO's pin table
RPItable2BP
	DCB	gpio_2
	DCB	gpio_3
	DCB	gpio_4
	DCB	gpio_14
	DCB	gpio_15
	DCB	gpio_17
	DCB	gpio_18
	DCB	gpio_27
	DCB	gpio_22
	DCB	gpio_23
	DCB	gpio_24
	DCB	gpio_10
	DCB	gpio_9
	DCB	gpio_25
	DCB	gpio_11
	DCB	gpio_8
	DCB	gpio_7
	DCB	gpio_0
	DCB	gpio_1
	DCB	gpio_5
	DCB	gpio_6
	DCB	gpio_12
	DCB	gpio_13
	DCB	gpio_19
	DCB	gpio_16
	DCB	gpio_26
	DCB	gpio_20
	DCB	gpio_21
;user bits table
	DCB	Pi_LED
	DCB	&FF
	ALIGN

;expansion GPIO's pin table
RPItable2Comp
	DCB	gpio_0
	DCB	gpio_1
	DCB	gpio_2
	DCB	gpio_3
	DCB	gpio_4
	DCB	gpio_5
	DCB	gpio_6
	DCB	gpio_7
	DCB	gpio_8
	DCB	gpio_9
	DCB	gpio_10
	DCB	gpio_11
	DCB	gpio_12
	DCB	gpio_13
	DCB	gpio_14
	DCB	gpio_15
	DCB	gpio_16
	DCB	gpio_17
	DCB	gpio_18
	DCB	gpio_19
	DCB	gpio_20
	DCB	gpio_21
	DCB	gpio_22
	DCB	gpio_23
	DCB	gpio_24
	DCB	gpio_25
	DCB	gpio_26
	DCB	gpio_27
	DCB	gpio_28
	DCB	gpio_29
	DCB	gpio_30
	DCB	gpio_31
	DCB	gpio_32
	DCB	gpio_33
	DCB	gpio_34
	DCB	gpio_35
	DCB	gpio_36
	DCB	gpio_37
	DCB	gpio_38
	DCB	gpio_39
	DCB	gpio_40
	DCB	gpio_41
	DCB	gpio_42
	DCB	gpio_43
	DCB	gpio_44
	DCB	gpio_45
;user bits table
	DCB	Com_LED
	DCB	&FF
	ALIGN

;expansion GPIO's pin table
RPItable2BP1
	DCB	gpio_2
	DCB	gpio_3
	DCB	gpio_4
	DCB	gpio_14
	DCB	gpio_15
	DCB	gpio_17
	DCB	gpio_18
	DCB	gpio_27
	DCB	gpio_22
	DCB	gpio_23
	DCB	gpio_24
	DCB	gpio_10
	DCB	gpio_9
	DCB	gpio_25
	DCB	gpio_11
	DCB	gpio_8
	DCB	gpio_7
	DCB	gpio_0
	DCB	gpio_1
	DCB	gpio_5
	DCB	gpio_6
	DCB	gpio_12
	DCB	gpio_13
	DCB	gpio_19
	DCB	gpio_16
	DCB	gpio_26
	DCB	gpio_20
	DCB	gpio_21
;user bits table
	DCB	Com_LED
	DCB	&FF
	ALIGN

;**************************** Raspberry End *************************************
;**************************** OMAP Start ****************************************
omap5table
	DCB	gpio_2
	DCB	gpio_3
	DCB	gpio_4
	DCB	gpio_13
	DCB	gpio_29
	DCB	gpio_30
	DCB	gpio_32
	DCB	gpio_33
	DCB	gpio_34
	DCB	gpio_35
	DCB	gpio_36
	DCB	gpio_37
	DCB	gpio_38
	DCB	gpio_39
	DCB	gpio_40
	DCB	gpio_42
	DCB	gpio_44
	DCB	gpio_45
	DCB	gpio_47
	DCB	gpio_50
	DCB	gpio_51
	DCB	gpio_54
	DCB	gpio_55
	DCB	gpio_56
	DCB	gpio_59
	DCB	gpio_61
	DCB	gpio_67
	DCB	gpio_68
	DCB	gpio_69
	DCB	gpio_70
	DCB	gpio_71
	DCB	gpio_72
	DCB	gpio_73
	DCB	gpio_74
	DCB	gpio_75
	DCB	gpio_76
	DCB	gpio_77
	DCB	gpio_78
	DCB	gpio_79
	DCB	gpio_80
	DCB	gpio_81
	DCB	gpio_82
	DCB	gpio_83
	DCB	gpio_96
	DCB	gpio_97
	DCB	gpio_98
	DCB	gpio_99
	DCB	gpio_110
	DCB	gpio_111
	DCB	gpio_112
	DCB	gpio_113
	DCB	gpio_119
	DCB	gpio_120
	DCB	gpio_134
	DCB	gpio_135
	DCB	gpio_136
	DCB	gpio_137
	DCB	gpio_138
	DCB	gpio_139
	DCB	gpio_140
	DCB	gpio_151
	DCB	gpio_152
	DCB	gpio_153
	DCB	gpio_154
	DCB	gpio_157
	DCB	gpio_172
	DCB	gpio_173
	DCB	gpio_174
	DCB	gpio_175
	DCB	gpio_176
	DCB	gpio_177
	DCB	gpio_178
	DCB	gpio_201
	DCB	gpio_202
	DCB	gpio_204
	DCB	gpio_205
	DCB	gpio_206
	DCB	gpio_207
	DCB	gpio_208
	DCB	gpio_209
	DCB	gpio_210
	DCB	gpio_233
	DCB	gpio_234
	DCB	gpio_236
	DCB	gpio_237
	DCB	gpio_238
	DCB	gpio_239
	DCB	gpio_240
	DCB	gpio_241
	DCB	gpio_242
	ALIGN

;**************************** OMAP End ******************************************

	END
