;
; Copyright (c) 2013, Tank Stage Lighting
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of the copyright holder nor the names of their
;       contributors may be used to endorse or promote products derived from
;       this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;

;	AREA	|Subs|, CODE, READONLY

doit
;r0=gpio
;r3=gadget
;r9=family
	Entry
	MOV	r5,r0			;save gpio
	SWI	GPIO_ReadData		;get state
	CMP	r0,#-1			;
	EXIT	EQ			;
	MOV	r6,r0			;keep output state
	MOV	r0,r5			;gpio
	SWI	GPIO_ReadOE		;
	MOV	r1,r0			;keep OE state
	CMP	r0,#0			;OE state
	MOVEQ	r4,#0			;green button
	MOVNE	r4,#1			;red button
	MOV	r0,r5			;
	SWI	GPIO_ReadMode		;get pin mode
	AND	r0,r0,#7		;just mode
	CMP	r9,#HALDeviceID_GPIO_BCM2835	;
	BEQ	%FT50			;skip safe if pi
	CMP	r0,#7			;safe mode
	MOVEQ	r4,#3			;safe button
50
	Push	"r0-r5"			;
	CMP	r1,#0			;in or out
	MOVEQ	r5,#10<<24		;colour border
	MOVNE	r5,#11<<24		;colour border
	CMP	r9,#HALDeviceID_GPIO_BCM2835	;
	BNE	%FT10			;
	CMP	r0,#0			;gpio mode
	CMPNE	r0,#1			;gpio mode
	B	%FT20
10	CMP	r9,#HALDeviceID_GPIO_OMAP4	;
	BNE	%FT30			;
	CMP	r0,#3			;gpio mode
	B	%FT20
30	CMP	r9,#HALDeviceID_GPIO_OMAP5	;
	BNE	%FT40			;
	CMP	r0,#6			;gpio mode
	B	%FT20
40	CMP	r0,#4			;OMAP3
20	MOVNE	r5,#7<<24		;ordinary border
	MOV	r0,#0			;setup call
	LDR	r1,mainwindow		;
	LDR	r2,buttonsetflag	;
	MOV	r4,#&F<<24		;
	SWI	Toolbox_ObjectMiscOp	;
	Pull	"r0-r5"			;
	MOV	r0,#0			;flags
	LDR	r1,mainwindow		;
	LDR	r2,setbutton		;set state
	ADRL	r5,icontable		;
	CMP	r6,#1			;
	CMPNE	r4,#3			;
	MOVNE	r4,#2			;
	MOV	r4,r4,LSL #2		;
	LDR	r4,[r5,r4]		;
	SWI	Toolbox_ObjectMiscOp	;
	EXIT

buttonsetflag	DCD	961
icontable
	DCD	on
	DCD	inputon
	DCD	off
	DCD	safe

;-----------------------------------------------------
getgpio
	Entry
	LDR	r3,iconstart		;first gadget
	ADRL	r0,boardfamily		;
	LDR	r9,[r0]			;
	ADRL	r0,iconend		;
	LDR	r8,[r0]			;
10	MOV	r2,r3			;
	BL	converticontogpio	;
	MOV	r0,r2			;
	CMP	r0,#&FF			;
	EXIT	EQ			;
	BL	doit			;
	CMP	r0,#-1			;error
	EXIT	EQ			;
	ADD	r3,r3,#1		;inc gadget counter
	CMP	r3,r8			;last one ?
	BLE	%BT10			;
	EXIT

;-----------------------------------------------------
;r2=icon number
converticontogpio
	Entry	"r3-r9"				;
	LDR	r3,boardtype			;get board table
	MOV	r3,r3,LSL #2			;
	ADRL	r4,non_table			;
	ADD	r3,r3,r4			;
	LDR	r3,[r3]				;
	LDRB	r2,[r3,r2]			;
	EXIT
;r2=gpio number
;-----------------------------------------------------
;r3=entry
;r5=gpio
;r6=menu title
getnamemode
	Entry	"r0-r3"				;
	ADR	r4,flt				;set as default text
	ADRL	r0,mainda+dareabase		;get description pointers
	LDR	r0,[r0]				;
	MOV	r2,r5,LSL#3			;
	ADD	r0,r0,r2			;point to data
	LDR	r11,[r0],#4			;save gpio
	CMP	r11,#-1				;last
	EXIT	EQ				;
	LDR	r2,[r0]				;get data pointer
	LDR	r1,[r2],#4			;get gpio phys number
	LDR	r1,[r2],#4			;get mask
20	LDRB	r1,[r2,#1]			;get mode
	CMP	r1,r3				;this mode?
	ADDNE	r2,r2,#4			;next mode
	BNE	%BT20				;
	LDRB	r1,[r2,#2]			;group type
	ADRL	r0,texttable			;make text
	ADD	r0,r0,r1,LSL #2			;
	LDR	r4,[r0]				;set pointer
	EXIT					;
;r4 text pointer

texttable
	DCD	gpio
	DCD	i2c
	DCD	gpclk
	DCD	spi
	DCD	pwm
	DCD	uart
	DCD	pcm
	DCD	bsc
	DCD	jtag
	DCD	usb
	DCD	led
	DCD	sw
	DCD	flt

gpio	DCB	"GPIO",0
	ALIGN
i2c	DCB	"I2C",0
	ALIGN
gpclk	DCB	"GPCLK",0
	ALIGN
spi	DCB	"SPI",0
	ALIGN
pwm	DCB	"PWM",0
	ALIGN
uart	DCB	"UART",0
	ALIGN
pcm	DCB	"PCM",0
	ALIGN
bsc	DCB	"BSC",0
	ALIGN
jtag	DCB	"JTAG",0
	ALIGN
usb	DCB	"USB",0
	ALIGN
led	DCB	"LED",0
	ALIGN
sw	DCB	"SW",0
	ALIGN
flt	DCB	"Fault",0
	ALIGN
gpioin	DCB	"GPIO IN",0
	ALIGN
gpioout	DCB	"GPIO OUT",0
	ALIGN
;-----------------------------------------------------

	END


