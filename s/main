;
; Copyright (c) 2013, Tank Stage Lighting
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of the copyright holder nor the names of their
;       contributors may be used to endorse or promote products derived from
;       this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;

	AREA	|!RunImage|, CODE, READONLY
	ENTRY

in
	SWI	OS_GetEnv		;get memory bits
	MOV	sp,r1			;setup stack pointer
	Push	"lr"
        ADRL    r0,dir			;r0=pointer to directory name
	ADRL    r1,name			;r1=pointer to block
        MOV     r2,#256			;length of block
        MOV     r3,#0			;First call
        MOV     r4,#3			;Expanded string returned
        SWI     OS_ReadVarVal		;Get string
        STR     r2,length		;Save length
        BL      setup			;Go to setup
	BL	task_setup		;task specific bits
	BL	mainin			;Main loop
	B	quit			;Quit application

setup
        Push	"lr"			;Stack lr
	MOV	r0,#0			;Toolbox flags
        MOV     r1,#250
	ADD	r1,r1,#100		;Last wimp version
        ADRL    r2,Wimpmask		;Pionter to wimp mask
	ADRL	r3,Toolboxmask		;pointer to toolbox mask
	ADRL	r4,name			;Pointer to Path name
	ADRL	r5,Messfiledes		;pointer to messagetrans block
	ADRL	r6,Toolboxblock		;Pointer to tbox block
        SWI     Toolbox_Initialise
        STR     r1,Task			;Wimp task ID
	STR	r2,Sprite		;Pointer to sprite block
        Pull	"pc"			;Pull pc from stack

mainin
        Push	"lr"			;Stack lr
	SWI	OS_ReadMonotonicTime
	ADD	r0,r0,#100		;Add aprox 1 second to time
	STR	r0,Count		;Save time
main_loop
	BL	updatedisplay		;update window
        LDR     r0,Wimpmask		;
        ADRL    r1,block		;Ponter to wimp block
        SWI     Wimp_Poll		;Poll wimp once every second
	CMP	r0,#&200		;Is it a toolbox event ?
	BEQ	more			;Yes, go to toolbox routine
        CMP     r0,#&13			;Is it outside wimps events
	BGE	main_loop		;Yes, loop to poll
        ADDLE   pc,pc,r0,LSL #2		;Get event branch
        B       main_loop		;Space filler
        B       main_loop		;Null code
        B       redraw			;Redraw window
        B       open			;Open window
        B       close			;Close window
        B       pointerl		;Pointer leaving window
        B       pointere		;Pointer entering window
        B       mouse			;Mouse click
        B       drag			;User drag box
        B       key			;Key pressed
        B       menu			;Menu selection
        B       scroll			;Scroll request
        B       lose			;Loose caret
        B       gain			;Gain catret
        B       nonzero			;Poll word non zero
        B       reserved		;Unused
        B       reserved		;Unused
        B       reserved		;Unused
        B       mess			;User message
        B       messr			;User message recorded
        B       messa			;User message acknowledge
        B       main_loop		;Loop to Poll

mess
messr

        LDR     r0,[r1,#16]		;Get Message type
        CMP     r0,#0			;Is it quit
        BEQ     closedown		;Yes, quit
	LDR	r2,tboxerr		;Get tollbox error no
	CMP	r0,r2			;Is it a toolbox error code
	BEQ	more			;Yes, go to toolbox routine
        B       main_loop		;Loop to poll

messa
        B       main_loop		;Loop to poll

closedown
closeapp
        Pull	"pc"			;Pull pc from stack(poll loop)


open
close
redraw
pointerl
pointere
drag
key
menu
scroll
lose
gain
nonzero
reserved
        B       main_loop		;Loop to poll

mouse
	ADRL	r3,Toolboxblock		;
	LDR	r4,[r3,#selfid]		;get tbox handle
	LDR	r2,mainwindow		;ours
	CMP	r4,r2			;
	BEQ	mousebutton		;
        B       main_loop		;Loop to poll


quit
	ADRL	r9,mainda		;kill dynamic area
	LDMIA	r9,{r0-r8}		;setup d area call
	MOV	r0,#1			;
	SWI	OS_DynamicArea		;
        LDR     r0,Task			;ro="TASK"
        LDR     r1,ta			;r1=Task ID
        SWI     Wimp_CloseDown		;Close wimp
        ADRL    r0,block		;r0=Pointer to block
	Pull	"pc"

more
 	LDR	r0,[r1,#8]		;Get toolbox event no
	CMP	r0,#&200		;Is it quit Option ?
	BEQ	closeapp		;Yes, quit program
	CMP	r0,#10
	BEQ	choosemaintoopen

	CMP	r0,#&100
	BEQ	mainopening
	CMP	r0,#&104
	BEQ	mainclosing
	CMP	r0,#&108
	BEQ	menuopening
	CMP	r0,#109
	BEQ	menuselectionmade

	LDR	r2,created
	CMP	r0,r2
	BEQ	maincreated

	B       main_loop		;Loop to poll

;-----------------------------------------------------
task_setup
	Push	"lr"
	MOV	r1,#0			;first call
	MOV	r0,#HALDeviceType_Comms	;comms
	ADD	r0,r0,#HALDeviceComms_GPIO	;gpio
	ADD	r0,r0,#&10000		;
	MOV	r8,#5			;OS_Hardware 5
	SWI	OS_Hardware		;call it
	LDRH	r1,[r2,#HALDevice_ID]	;get processor
	STR	r1,boardfamily		;
	MOV	r0,#9			;read platform name
	MOV	r1,#7			;
	SWI	OS_ReadSysInfo		;
	CMP	r0,#0			;
	Pull	"pc",EQ			;
	ADRL	r1,halname		;
	STR	r0,halname		;
	ADRL	r5,nametable		;get board type
	ADD	r5,r5,#4		;skip non detected
10	LDR	r0,[r1]			;
	LDR	r2,[r5]			;
	CMP	r2,#-1			;non detected
	ADREQL	r5,nametable		;
	BEQ	%FT30			;out
20	LDRB	r3,[r2],#1		;
	LDRB	r4,[r0],#1		;
	CMP	r3,#0			;found
	BEQ	%FT30			;
	CMP	r4,r3			;
	ADDNE	r5,r5,#4		;
	BNE	%BT10			;next board name
	B	%BT20			;next character
30	ADRL	r2,nametable		;
	SUB	r5,r5,r2		;
	MOV	r0,r5,LSR#2		;
	CMP	r0,#lasttype		;from reals
	MOVGT	r0,#0			;non detected
	ADRL	r1,boardtype		;
	STR	r0,[r1]			;
	ADRL	r9,mainda		;setup dynamic area
	LDMIA	r9,{r0-r8}		;setup d area call
	SWI	OS_DynamicArea		;
	STMIA	r9,{r0-r8}		;setup d area info
	LDR	r9,[r9,#dareabase]	;
	MOV	r1,#0			;get pin info
	MOV	r2,#0			;
	MOV	r4,#0			;pin count
	MOV	r5,#0			;zero
	MOV	r6,#0			;icon count
40	LDR	r0,full			;
	SWI	GPIO_Info		;
	CMP	r1,#-1			;last
	BEQ	%FT50			;
45	STR	r4,[r9],#4		;save GPIO number
	CMP	r3,r4			;
	STREQ	r0,[r9],#4		;save pointer to pin info
	STRNE	r5,[r9],#4		;
	ADDEQ	r6,r6,#1		;icon count
	ADD	r4,r4,#1		;inc pin count
	BNE	%BT45			;loop until equal
	B	%BT40			;get next
50	STR	r4,[r9],#4		;save GPIO number
	STR	r0,[r9],#4		;save pointer to pin info
	STR	r1,[r9],#4		;save terminator
	STR	r1,[r9],#4		;save terminator
	STR	r6,iconend		;
	Pull	"pc"

full	DCB	"FULL"			;
	ALIGN
;-----------------------------------------------------
choosemaintoopen
	ADRL	r0,boardtype		;
	LDR	r0,[r0]			;
	MOV	r0,r0,LSL #2		;
	ADRL	r3,matchtable		;
	ADD	r1,r0,r3		;pointer to name
	MOV	r0,#0			;flage
	SWI	Toolbox_CreateObject	;
	MOV	r1,r0			;id
	STR	r1,mainwindow		;save handle
	MOV	r0,#0			;flags
	MOV	r2,#0			;default
	MOV	r3,#0			;
	MOV	r4,#0			;
	MOV	r5,#0			;
	SWI	Toolbox_ShowObject	;
	MOV	r0,#0			;flags
	LDR	r1,mainwindow		;id
	MOV	r2,#0			;
	SWI	Toolbox_ObjectMiscOp	;get wimp handle
	STR	r0,realwindow		;store it
	MOV	r0,#0			;flags
	LDR	r1,mainwindow		;id
	MOV	r2,#11			;set title
	ADRL	r3,halname		;
	LDR	r3,[r3]			;
	SWI	Toolbox_ObjectMiscOp	;
	B	main_loop
;-----------------------------------------------------
mainopening
	LDR	r0,mainflag		;get flags
	ORR	r0,r0,#mainwindowflag	;set bit
	STR	r0,mainflag		;save handle
	BL	maindisp
	B       main_loop

;-----------------------------------------------------
mainclosing
	LDR	r0,mainflag		;get flags
	BIC	r0,r0,#mainwindowflag	;clear bit
	STR	r0,mainflag		;save handle
	B       main_loop

;-----------------------------------------------------
maincreated
	MOV	r0,#0			;set up call
	LDR	r5,[r1,#16]		;first 4 letters of name
	ADRL	r3,Toolboxblock		;
	LDR	r1,[r3,#selfid]		;get tbox handle
	LDR	r3,menutitle		;compare name
	CMP	r5,r3			;
	STREQ	r1,menuwindow		;save handle
;next
	B       main_loop
;-----------------------------------------------------
menuopening
	LDR	r6,menuwindow		;
	MOV	r0,#0			;set tick
	MOV	r1,r6			;
	MOV	r2,#0			;
	ADRL	r3,currentmode		;
	LDR	r3,[r3]			;
	MOV	r4,#1			;
	SWI	Toolbox_ObjectMiscOp	;
	ADRL	r1,currentpin		;
	LDR	r0,[r1]			;
	CMP	r0,#256			;
	BGT	main_loop
	ADRL	r1,datastore		;temp storage
	MOV	r2,#128			;size
	SWI	OS_ConvertInteger2	;convert pin number to string
	MOV	r3,r0			;
	MOV	r0,#0			;
	MOV	r1,r6			;
	MOV	r2,#24			;
	SWI	Toolbox_ObjectMiscOp	;set title
	B       main_loop

;-----------------------------------------------------
menuselectionmade
	LDR	r6,menuwindow		;
	MOV	r0,#0			;unset tick
	MOV	r1,r6			;
	MOV	r2,#0			;
	ADRL	r3,currentmode		;
	LDR	r3,[r3]			;
	MOV	r4,#0			;
	SWI	Toolbox_ObjectMiscOp	;
	ADRL	r0,Toolboxblock		;
	LDR	r3,[r0,#selfgadget]	;get gadget selected
	ADRL	r2,currentmode		;
	STR	r3,[r2]			;store as current mode
	MOV	r0,#0			;set tick
	MOV	r1,r6			;
	MOV	r2,#0			;
	MOV	r4,#1			;
	SWI	Toolbox_ObjectMiscOp	;
	ADRL	r1,currentpin		;
	LDR	r0,[r1]			;
	MOV	r5,r0			;
	SWI	GPIO_ReadMode		;get mode
	BIC	r0,r0,#7		;clear mode
	ADRL	r2,currentmode		;
	LDR	r3,[r2]			;store as current mode
	ORR	r1,r0,r3		;set new mode
	MOV	r0,r5			;
	SWI	GPIO_WriteMode		;set mode
	B       main_loop
;-----------------------------------------------------
mousebutton
	ADRL	r0,boardfamily		;
	LDR	r4,[r0]			;
	CMP	r4,#-1			;none
	BEQ	main_loop		;get out
	ADRL	r5,iconend		;
	LDR	r5,[r5]			;
	LDR	r2,[r3,#selfgadget]	;get gadget clicked
	CMP	r2,#-1			;
	BEQ	main_loop		;get out
	CMP	r2,r5			;in range ?
	BGT	main_loop		;get out
	BL	converticontogpio	;
	ADRL	r7,mainda		;
	LDR	r7,[r7,#dareabase]	;convert to GPIO data
	ADD	r7,r7,r2,LSL #3		;
	LDR	r7,[r7,#4]		;
	CMP	r7,#0			;not available
	BEQ	main_loop		;get out
	ADRL	r8,currentpin		;
	STR	r2,[r8]			;save to in use store
	LDR	r8,[r7,#modesmask]	;also get mode data
	CMP	r4,#HALDeviceID_GPIO_BCM2835	;add mode 1 to pi if mode 0
	BNE	%FT10			;
	TST	r8,#1			;test for GPIO in on pi
	ORRNE	r8,r8,#2_10		;add GPIO out as available
10	MOV	r5,r4			;fam int r5
	LDR	r4,[r1,#8]		;get button type
	CMP	r4,#4			;select
	BEQ	toggleupdown		;
	CMP	r4,#2			;menu
	BEQ	modemenu		;
	CMP	r4,#1			;adjust
	BEQ	toggleio		;
	B       main_loop
;-----------------------------------------------------
;r2=gpio
;r5=family
;r8=modemask
toggleupdown
	MOV	r0,r2			;get gpio
	CMP	r5,#HALDeviceID_GPIO_BCM2835	;
	BEQ	pitoggleud		;pi
	SWI	GPIO_ReadMode		;get mode
	AND	r4,r0,#7		;just mode
	CMP	r5,#HALDeviceID_GPIO_OMAP3	;
	CMPEQ	r4,#4			;is it GPIO
	BEQ	%FT10			;
	CMP	r5,#HALDeviceID_GPIO_OMAP4	;omap4
	CMPEQ	r4,#3			;is it GPIO
	CMP	r5,#HALDeviceID_GPIO_OMAP5	;omap5
	CMPEQ	r4,#6			;is it GPIO
	BNE	main_loop		;
10	MOV	r0,r2			;get gpio
	SWI	GPIO_ReadOE		;get OE
	CMP	r0,#0			;output ?
	MOV	r0,r2			;get gpio
	BEQ	togglepin		;then toggle it
	SWI	GPIO_ReadMode		;get mode
	MOV	r1,r0			;
	TST	r0,#pullenable		;check existing
	ORREQ	r1,r1,#pullenable	;set bit
	TST	r0,#pulltype		;check existing
	ORREQ	r1,r1,#pulltype		;set bit
	BICNE	r1,r1,#pulltype		;unset bit
	MOV	r0,r2			;get gpio
	SWI	GPIO_WriteMode		;set mode
	B       main_loop

togglepin
	MOV	r2,r0			;
	SWI	GPIO_ReadData		;
	CMP	r0,#0			;
	MOVEQ	r1,#1			;
	MOVNE	r1,#0			;
	MOV	r0,r2			;
	SWI	GPIO_WriteData		;
	B       main_loop


toggleio
	MOV	r0,r2			;get gpio
	SWI	GPIO_ReadMode		;get mode
	AND	r0,r0,#7		;just mode
	CMP	r5,#HALDeviceID_GPIO_OMAP3	;
	CMPEQ	r0,#4			;is it GPIO
	BEQ	%FT60			;
	CMP	r5,#HALDeviceID_GPIO_OMAP4	;
	CMPEQ	r0,#3			;is it GPIO
	BEQ	%FT60			;
	CMP	r5,#HALDeviceID_GPIO_OMAP5	;
	CMPEQ	r0,#6			;is it GPIO
	BEQ	%FT60			;
	CMP	r5,#HALDeviceID_GPIO_BCM2835	;
	CMPEQ	r0,#1			;is it GPIO
	BEQ	%FT60			;
	CMP	r5,#HALDeviceID_GPIO_BCM2835	;
	CMPEQ	r0,#0			;is it GPIO
	BEQ	%FT60			;
	B	main_loop		;
60	MOV	r0,r2			;get gpio
	SWI	GPIO_ReadOE		;
	CMP	r0,#0			;in or out ?
	MOVEQ	r1,#1			;swap
	MOVNE	r1,#0			;
	MOV	r0,r2			;get gpio
	SWI	GPIO_WriteOE		;set
	B       main_loop

modemenu
	MOV	r10,r5			;keep family
	LDR	r6,menuwindow		;
	MOV	r5,r2			;get gpio
	MOV	r0,r5			;
	SWI	GPIO_ReadMode		;get mode
	AND	r0,r0,#7		;just mode
	MOV	r7,r0			;save for later
	ADRL	r1,currentmode		;
	STR	r0,[r1]			;save to in use store
	MOV	r0,#1			;as menu
	MOV	r1,r6			;get handle
	MOV	r2,#4			;at pointer
	MOV	r3,#0			;
	LDR	r4,mainwindow		;
	SWI	Toolbox_ShowObject	;
	MOV	r3,#0			;entry count
	MOV	r9,r8			;mode mask
5	MOV	r0,r3			;make entry text
	BL	getnamemode		;
	ADRL	r1,menunumber		;buffer
	MOV	r2,#10			;size
	SWI	OS_ConvertCardinal1	;
	MOV	r0,#0			;reset entries
	MOV	r1,r6			;
	MOV	r2,#4			;
	MOVS	r9,r9,LSR #1		;check if mode available
	ADRCCL	r4,menunumber		;use mode num if not available
	CMP	r10,#HALDeviceID_GPIO_BCM2835	;check if pi mode window
	CMPEQ	r3,#0			;
	ADREQL	r4,gpioin		;
	CMP	r10,#HALDeviceID_GPIO_BCM2835	;check if pi mode window
	CMPEQ	r3,#1			;
	ADREQL	r4,gpioout		;
	SWI	Toolbox_ObjectMiscOp	;
	ADD	r3,r3,#1		;
	CMP	r3,#7			;
	BLE	%BT5			;
	MOV	r3,#0			;
10	MOV	r0,#0			;unset tick
	MOV	r1,r6			;
	MOV	r2,#0			;
	CMP	r3,r7			;
	MOVNE	r4,#0			;
	MOVEQ	r4,#1			;set this mode
	SWI	Toolbox_ObjectMiscOp	;
	ADD	r3,r3,#1		;
	CMP	r3,#7			;
	BLE	%BT10			;
	MOV	r3,#0			;
20	MOV	r0,#0			;fade
	MOV	r1,r6			;
	MOV	r2,#2			;
	MOVS	r8,r8,LSR #1		;
	MOVCS	r4,#0			;
	MOVCC	r4,#1			;set this fade
	SWI	Toolbox_ObjectMiscOp	;
	ADD	r3,r3,#1		;
	CMP	r3,#7			;
	BLE	%BT20			;
	MOV	r0,r5			;make GPIO pin text
	ADRL	r1,menunumber		;
	MOV	r2,#10			;
	SWI	OS_ConvertCardinal1	;
	MOV	r0,#0			;set menu title
	MOV	r1,r6			;
	MOV	r2,#24			;
	ADRL	r3,menutitletext	;
	SWI	Toolbox_ObjectMiscOp	;
	B       main_loop

pitoggleud
	SWI	GPIO_ReadMode		;get mode
	AND	r1,r0,#7		;just mode
	CMP	r1,#2			;is it pi GPIO in/out
	BGE	main_loop		;
	CMP	r1,#1			;output ?
	MOVEQ	r0,r2			;get gpio
	BEQ	togglepin		;then toggle it
	TST	r0,#pullenable		;check existing
	ORREQ	r1,r1,#pullenable	;set bit
	TST	r0,#pulltype		;check existing
	ORREQ	r1,r1,#pulltype		;set bit
	BICNE	r1,r1,#pulltype		;unset bit
	MOV	r0,r2			;get gpio
	SWI	GPIO_WriteMode		;set mode
	B       main_loop

;-----------------------------------------------------
updatedisplay
	Push	"lr"
	LDR	r0,mainflag		;check if any open
	CMP	r0,#0			;
	Pull	"pc",EQ			;
	SWI	OS_ReadMonotonicTime	;
	LDR	r1,Count		;
	CMP	r0,r1			;
	Pull	"pc",LT
	BL	do_mimic
	SWI	OS_ReadMonotonicTime	;
        ADD     r0,r0,#5		;Add
        STR     r0,Count		;Save
	Pull	"pc"

do_mimic
	Push	"lr"
	LDR	r0,mainflag		;check which open
	TST	r0,#mainwindowflag	;
	BLNE	maindisp		;
	Pull	"pc"

maindisp
	Push	"lr"
	ADRL	r0,boardfamily		;
	LDR	r0,[r0]			;
	CMP	r0,#-1			;none
	Pull	"pc",EQ			;get out

;get and display table
	BL	getgpio
	Pull	"pc"
;-----------------------------------------------------


	END
